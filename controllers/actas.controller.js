'use strict'
let fs = require('fs');
let path = require('path');
let Global = require('../global')
var request = require('request');

const canal = 'nuevapublicacion';

function obtenerArchivo(req, res) {

    let imagen_file = req.query.archivo;
    let path_file = './uploads/' + imagen_file;
    if (path_file.length > 0) {
        fs.exists(path_file, function (exists) {
            if (exists) {
                res.sendFile(path.resolve(path_file));
            } else {
                res.status(200).send({ message: 'No existe la imagen' });
            }
        });
    } else {
        res.status(200).send({ message: 'No solicit� un archivo' });
    }

}


function agregarArchivo(req, res) {
    console.log(req.files);

    let infoAdicional = req.body;
    let params = req;
    let files = params.files;
    let patt = files.archivo.path;
    let file_name = 'sin imagen';
    let hashfile = '';

    if (patt) {

        let file_path = patt;
        let file_split = '';
        if(file_path.indexOf('\/') > -1){
            file_split = file_path.split('\/');
        } else {
            file_split = file_path.split('\\');
        }       

        file_name = file_split[1];
        let ext_split = file_name.split('.');
        let file_ext = ext_split[1];

        //Renombrado de los Archivos
        // fs.exists(file_path, function (exists) {
        fs.stat(file_path, function (error, exists) {

            if (exists) {

                let newFilePath = file_split[0] + "/" + infoAdicional.nombreOriginal;

                fs.rename(file_path, newFilePath, function (err) {
                    if (err) { console.log(err); return; }

                    //Bloque de Imagenes       
                    if (file_ext == 'jpg') {
                        console.log("--------------------------------------------------------------------");
                        console.log("---- Acta Recibida: " + infoAdicional.nombreOriginal);
                        console.log("--------------------------------------------------------------------");
                        
                        var formData = {
                            archivo: {
                                value: fs.createReadStream(newFilePath),
                                options: {
                                    filename: infoAdicional.nombreOriginal,
                                    contentType: 'image/jpeg'
                                }
                            }
                        };

                        request.post({ url: Global.global.urlFiles, formData: formData }, function optionalCallback(err, httpResponse, body) {
                            if (err) {
                                return console.error('upload failed:', err);
                            }
                        });

                        res.status(200).send({ message: 'Archivo Recibido'});
                    }

                    //Bloque de archivos .tar.gz, 
                    else if (file_ext == 'gz' || file_ext == 'zip' ) {
                        console.log("--------------------------------------------------------------------");
                        console.log("---- Archivo de Base de Datos: " + infoAdicional.nombreOriginal);
                        console.log("--------------------------------------------------------------------");
                        var formData = {
                            archivo: {
                                value: fs.createReadStream(newFilePath),
                                options: {
                                    filename: infoAdicional.nombreOriginal,
                                    contentType: 'application/x-tar'
                                }
                            }
                        };

                        request.post({ url: Global.global.urlFiles, formData: formData }, function optionalCallback(err, httpResponse, body) {
                            if (err) {
                                return console.error('upload failed:', err);
                            }
                        });


                        res.status(200).send({ message: 'Archivo Recibido'});
                    }
                    //Bloque Rechazar
                    else {
                        console.log("\n--------------------------------------------------------------------");
                        console.log("---- Archivo Rechazaado: " + file_name);
                        console.log("--------------------------------------------------------------------\n");
                        res.status(400).send({ message: 'Archivo Recibido y Rechazado'});
                    }


                });

            } else {
                statusRenombrar = false;
                console.log("El archivo NO Existe!!!");
                console.log("\n--------------------------------------------------------------------");
                console.log("---- Archivo Rechazaado: " + file_name);
                console.log("--------------------------------------------------------------------\n");
                res.status(400).send({ message: 'Archivo No se Recibio'});
            }

        });
    }
}

function getFilesizeInBytes(filename) {
    const stats = fs.statSync(filename)
    const fileSizeInBytes = stats.size
    return fileSizeInBytes
}

module.exports = {
    agregarArchivo,
    obtenerArchivo
    //prueba
}