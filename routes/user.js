'use strict'

let express = require('express');
//let UserController = require('../controllers/usuariosApp.controller');
let api = express.Router();
//let md_auth = require('../middlewares/autenticacion');
let multipart = require('connect-multiparty');
let md_upload = multipart({ uploadDir: './uploads' });


let informacion= require('../controllers/informacion.controller');
let actas= require('../controllers/actas.controller');

api.post('/informacion', md_upload, informacion.aregarInformacion);
api.post('/archivos', md_upload, actas.agregarArchivo);

//api.post('/informacion',  archivos.aregarInformacion);
//api.post('/archivos', actas.agregarArchivo);

api.get('/consultar',actas.obtenerArchivo);

module.exports = api;