'use strict'
var bcrypt = require('bcrypt-nodejs');
var UsuarioAPP = require('../models/usuarioApp.model');
var jwt = require('../services/jwt');

function prueba(req, res) {
    res.status(200).send('prueba');
}
 function convierte_fecha(){

}
function loginUsuario(req, res) {    
    let params = req.body;
    let Nombre = params.Nombre;
    let pass = params.password;    
    UsuarioAPP.findOne({ 'Nombre': Nombre}, (err, user) => {
        if (err) {
            res.status(500).send({ message: 'Error en la petición de acceso' });
        } else {
            if (!user) {
                res.status(400).send({ message: 'El usuario no existe' });
            } else {
                bcrypt.compare(pass, user.Password, (err, check) => {
                    if (check) {

                        res.status(200).send({ 
                            user:{
                                _id:user._id,
                                Nombre:user.Nombre,
                                Password:user.Password,                                
                                Rol:user.Rol,
                                Activo:user.Activo,
                                Identificador_SIEE:user.Identificador_SIEE,
                                Estatus:user.Estatus,
                                Sincronizar:user.Sincronizar                        
                            } ,
                            token: jwt.createToken(user)
                        });

                        //if (params.gethash) {
                          //  console.log('se obtuvo el token');
                        //    res.status(200).send({
                        //        token: jwt.createToken(user)
                        //    })
                        //} else {
                        //    console.log(user);
                        //    res.status(200).send({ user:user });
                        //}
                    } else {
                        res.status(404).send({ message: 'El usuario no se a podido loguear' })
                    }
                });
            }
        }
    });
};


function altausuarioprueba(req,res){    
    let usuario= new UsuarioAPP();
    usuario.IdUsuario='1';
    usuario.Nombre='admin';
    //usuario.Password=''
    usuario.Fecha_Creacion= new Date();
    usuario.Rol='Administrador';
    usuario.Activo=true;
    usuario.Identificador_SIEE='1';
    usuario.Estatus='Inicial';
    usuario.Sincronizar=true;

    bcrypt.hash('admin', null, null, function(err, hash) {
        usuario.Password = hash;
        if (usuario.Nombre != null && usuario.Rol != null) {
            usuario.save((err, usuarioStored) => {
                if (err) {
                    return res.status(500).send({ message: 'Error al registrar el usuario' });
                } else {
                    if (!usuarioStored) {
                        return res.status(400).send({ message: 'No se ha registrado el usuario' });
                    } else {
                     return res.status(200).send({ usuario: usuarioStored });
                    }
                }
            });
        } else {
            return res.status(200).send({ message: 'Introdusca todos los campos' });
        }
    });
  
}

module.exports = {   
    loginUsuario,
    prueba,
    altausuarioprueba
};