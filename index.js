'use strict'
var app = require('./app');


var port = process.env.PORT || 35200
app.listen(port, function () {
    console.log('Servidor de api rest escuchando en el puerto: ' + port);
});
