'use strict'
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var CasillasSchema = Schema({
    ProcesoElectoral:String,
    IdentificacionCasilla:{
        Entidad_Federativa:String,
        Distrito_Electoral:String,
        Seccion_Electoral:String,
        Tipo_Casilla:String,
        Numero_Casilla:String,
        Municipo_Alcaldia:String
    },
    Identificacion_SIEE:String,        
    UsuarioAsignado:String,
    HoraEnvio:String,
    FechaEnvio:String,
    Actas:[{
        Entidad_Federativa:String,
        Distrito_Electoral:String,
        Seccion_Electoral:String,        
        Tipo_Eleccion:String,    
        Hora_Envio:Date,
        Fecha_Envio:Date,
        Nombre_Archivo_Enviado:String,
        Nombre_Archivo_Guardado:String,
        Hash_Recibido:String,
        Hash_generado:String,
        Detalle_Archivo:{
            Extension:String,
            Tam_Archivo:String        
        },
        Ubicacion:{
            latitud:Number,
            longitud:Number
        }
     }]
});

module.exports = mongoose.model('Casilla', CasillasSchema);