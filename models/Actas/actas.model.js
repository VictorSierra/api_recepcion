'use strict'
let mongoose = require('mongoose');
let Schema = mongoose.Schema;
let ActaSchema= Schema({
        Id_Casilla:[Schema.Types.ObjectId],
        Entidad_Federativa:String,
        Distrito_Electoral:String,
        Seccion_Electoral:String,        
        Tipo_Eleccion:String,    
        Hora_Envio:Date,
        Fecha_Envio:Date,
        Nombre_Archivo_Enviado:String,
        Nombre_Archivo_Guardado:String,
        Hash_Recibido:String,
        Hash_generado:String,
        Detalle_Archivo:{
            Extension:String,
            Tam_Archivo:String        
        },
        Ubicacion:{
            latitud:Number,
            longitud:Number
        }
});
module.exports = mongoose.model('Actas', ActaSchema);